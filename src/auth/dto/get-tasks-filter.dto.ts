
import { IsOptional, IsIn, IsNotEmpty } from 'class-validator';

export class GetTasksFilterDto {


    @IsOptional()
    @IsNotEmpty()
    event_title:string;

    @IsOptional()
    @IsNotEmpty()
    starts:string;
 
    @IsOptional()
    @IsNotEmpty()
    event_type:string;

    
   
    @IsOptional()
    @IsNotEmpty()
    location:string;



   

}