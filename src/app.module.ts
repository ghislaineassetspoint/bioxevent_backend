import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';
import { EventModule } from './event/event.module';
//import { MailgunModule } from '@nextnm/nestjs-mailgun';
import { MailgunModules } from './mailgun/mailgun.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { MessageModule } from './message/message.module';


@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    
    AuthModule,
    EventModule,
    MailgunModules,
    ServeStaticModule.forRoot({ rootPath: `${process.cwd()}/public` }),
    MessageModule,
    
    
],
})
export class AppModule {}
