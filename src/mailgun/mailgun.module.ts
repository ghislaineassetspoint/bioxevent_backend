import { Module } from '@nestjs/common';
import { MailgunModule } from '@nextnm/nestjs-mailgun';
import { MailgunServices } from './mailgun.service';
import { ForgottenRepo } from 'src/auth/forgottenpassword.repo';

@Module({
    imports: [
        MailgunModule.forRoot({
          DOMAIN: 'sandboxf638aa16cb204884b4e27b44f6051a59.mailgun.org',
          AKI_KEY: 'ae5360894b10a479e57982d1d89dcc0a-1b6eb03d-a14520bf',
        }),
      ],
      
      exports: [],
    
  providers: [MailgunServices, ForgottenRepo]
})

export class MailgunModules {}
