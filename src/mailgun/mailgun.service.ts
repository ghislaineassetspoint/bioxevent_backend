import { Injectable } from '@nestjs/common';
import { MailgunService } from  '@nextnm/nestjs-mailgun';
import { InjectRepository } from '@nestjs/typeorm';
import { ForgottenRepo } from 'src/auth/forgottenpassword.repo';
//import { ForgottenPassword } from 'src/auth/interfaces/forgottenpassword.interface';
import { ForgottenPassword } from "../auth/forgottenpassword.entity";
import { HttpException, HttpStatus } from "@nestjs/common";

@Injectable()
export class MailgunServices {
    constructor(  @InjectRepository( ForgottenRepo)
      private forgottenpassword: ForgottenRepo,
      private mailgunService: MailgunService) {

        

/*
        const options: EmailOptions = {
          from: 'cireguisse2015@gmail.com',
          to: 'cireguisse2015@gmail.com',
          subject: 'test',
          text: 'google.com',
          //html: 'g',
          //attachment:'df',
        };
         this.mailgunService.sendEmail(options);*/
         
    }
   /* async createForgottenPasswordToken(email: string){
      return await this.forgottenpassword.createForgottenPasswordToken(email);
    };
    async verifyEmail () {
      await this.mailgunService.verifyEmail('next@examle.com);
    }*/

    async createForgottenPasswordToken(email: string): Promise <ForgottenPassword> {
      let  forgottenPassword= await this.forgottenpassword.findOne({email: email});
      if (forgottenPassword && ( (new Date().getTime() - forgottenPassword.timestamp.getTime()) / 60000 < 15 )){
        throw new HttpException('RESET_PASSWORD.EMAIL_SENDED_RECENTLY', HttpStatus.INTERNAL_SERVER_ERROR);
      } else {
        const options: EmailOptions = {
          from: 'cireguisse2015@gmail.com',
          to: 'cireguisse2015@gmail.com',
          subject: 'test',
          text: 'google.com',
          //html: 'g',
          //attachment:'df',
        };
         this.mailgunService.sendEmail(options);

         if( forgottenPassword ){

            return forgottenPassword;

          } else {
            throw new HttpException('LOGIN.ERROR.GENERIC_ERROR', HttpStatus.INTERNAL_SERVER_ERROR);
          }
      }
    return ;
}
}
        

