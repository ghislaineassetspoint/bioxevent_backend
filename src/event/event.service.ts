import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EventRepository } from './event.repository';
import { SessionRepository } from './sessions.repository';
import { TicketsRepository } from './tickets.repository';
import { CreateEventDto } from 'src/auth/dto/create-event.dto';
import {Event} from 'src/event/event.entity';
import { GetEventsDto } from 'src/auth/dto/get-event.dto';
import { Individuals } from 'src/auth/user.entity';
import { GetTasksFilterDto } from 'src/auth/dto/get-tasks-filter.dto';

@Injectable()
export class EventService {
    constructor(
        @InjectRepository(EventRepository)
        @InjectRepository(SessionRepository)
        @InjectRepository(TicketsRepository)
        private eventDto : EventRepository,
        )   
    {}
    async createEvent(
       createEventDto : CreateEventDto,
       individual:Individuals,
       ): Promise<Event> {
       
        return this.eventDto.createEvent(createEventDto , individual);
      }

      async getEvent(
        listingEvents: GetEventsDto,
        individual:Individuals,): Promise<Event []> {
        return this.eventDto.getEvent(listingEvents,individual);
      }
      
      async getEventById(
        id: number,
        individual : Individuals): Promise<Event> {
        const found = await this.eventDto.findOne( {where :{id, individualId : individual.id}});
    
        if (!found) {
          throw new NotFoundException(`Event with ID "${id}" not found`);
        }
    
        return found;
      }

      async searchEvent(filterDto: GetTasksFilterDto): Promise<Event[]> {
        return this.eventDto.searchEvent(filterDto);
      }
    

      async updateEvents( 
        id: number, event_title, location, starts,ends,event_description,event_type,event_topic, event_image  : CreateEventDto,
        individual : Individuals): Promise<Event> {
        
        const event = await this.getEventById(id,individual);
        event.event_title = event_title ;
        event.location = location;
        event.starts = starts;
        event.ends = ends;
        event.event_description = event_description;
        event.event_topic = event_topic;
        event.event_type = event_type ;
        
        await event.save();
        return event;
      }

      async deleteEvent(
        id: number,
        individual : Individuals,
        ): Promise<void> {
        const result = await this.eventDto.delete( {id, individualId : individual.id});
  
        if (result.affected === 0) {
          throw new NotFoundException(`Task with ID "${id}" not found`);
        }
      }
 // Gerer la visibiliter lors de la partici^pation a un evenement
 /*
   si les infos du user sont enregistrer sur un BD u ajoute un champ status (true or false) if(true) showUser() else{hideUser()}

   si les infos du user sont enregistrer sur un BD u ajoute un champ status (true or false) if(true) showUser() else{hideUser()}

    SchalckeAujourd’hui à 17:14
  tu crée tes méthodes showUser() et hideUser() que tu vas call en fonction d'une action genre un bouton radio (déjà ça résout le comportement Boolean voulu) si le bouton est checked tu show ton user sinon tu le hide
 *//*
      async  showUser (statusUser: boolean) : Promise <boolean>{
           listingParticipants: GetEventsDto,
           individual:Individuals,): Promise<Event[]> {
           return this.eventDto.getEvent(listingPartipants,individual);
      }
        
      }

      async  HideUser (statusUser: boolean) : Promise <boolean>{

         
      }
      */
     /*
     export class EventRepository extends Repository<Event> {
  async getEvents(filterDto: GetEventsFilterDto): Promise<Events[]> {
    const { status, search } = filterDto;
    const query = this.createQueryBuilder('event');

    if (status) {
      query.andWhere('event.status = :status', { status });
    }

    if (search) {
      query.andWhere('(event.title LIKE :search OR event.description LIKE :search)', { search: `%${search}%` });
    }

    const events = await query.getMany();
    return events;
  }
  async getEvents(filterDto: GetEventsFilterDto): Promise<Event[]> {
    return this.eventRepository.getEvents(filterDto);
  }
  */
  
}
