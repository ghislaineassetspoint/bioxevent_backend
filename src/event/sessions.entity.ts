import { BaseEntity, Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { Individuals } from "src/auth/user.entity";
import {Event } from "src/event/event.entity";

@Entity()
export class Sessions extends BaseEntity{
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    name:string;

    @Column()
    starts: string;

    @Column()
    ends: string;
    
    @Column()
    day:string;


    @ManyToOne(type=> Individuals, individual=> individual.session, { eager: false})
    individual:Individuals;

    @Column()
    individualId : number;

    @ManyToOne(type=> Event, event=> event.session, { eager: false})
    event:Event;

    @Column()
    eventId : number;


}